﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_tbl_splittings.Entity
{
    public class UserCommunicationEntity
    {
        public int? UserId { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}